// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "GGJ2019HUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "UI/View/MainView.h"
#include "GGJ2019Character.h"

AGGJ2019HUD::AGGJ2019HUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
}

void AGGJ2019HUD::BeginPlay()
{
	Super::BeginPlay();
    if (ensureAlways(MainViewClass))
    {
	mMainView = CreateWidget<UMainView>(GetOwningPlayerController(), MainViewClass);
	mMainView->AddToViewport(1);

    if (auto OwningPlayer = Cast<AGGJ2019Character>(GetOwningPawn()))
    {
        OwningPlayer->OnDeath()->AddLambda([this, OwningPlayer](AActor*)
        {
            const float MaxHealth = OwningPlayer->GetMaxHealth();
            SetHealth(FText::FromString(FString::Printf(TEXT("%i/%i"), 0, (int)MaxHealth)));
            SetHealth(0);
        });
        OwningPlayer->OnDamaged()->AddLambda([this, OwningPlayer](float, AActor*, AActor*)
        {
            const float CurHealth = OwningPlayer->GetCurHealth();
            const float MaxHealth = OwningPlayer->GetMaxHealth();
            SetHealth(FText::FromString(FString::Printf(TEXT("%i/%i"), (int)CurHealth, (int)MaxHealth)));
            SetHealth(CurHealth / MaxHealth);
        });
        const float CurHealth = OwningPlayer->GetCurHealth();
        const float MaxHealth = OwningPlayer->GetMaxHealth();
        
        SetHealth(CurHealth / MaxHealth);
        SetHealth(FText::FromString(FString::Printf(TEXT("%i / %i"), (int)CurHealth, (int)MaxHealth)));
        }
        else
        {
            SetHealth(1);
            SetHealth(FText::FromString(FString::Printf(TEXT("%i / %i"), 100, 100)));
        }
    }
}

void AGGJ2019HUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void AGGJ2019HUD::SetHealth(FText&& HealthText)
{
	if (mMainView)
	{
		mMainView->SetHealthText(HealthText);
	}
}

void AGGJ2019HUD::SetHealth(float HealthPercentage)
{
	if (mMainView)
	{
		mMainView->SetHealthBarValue(HealthPercentage);
	}
}
/*
void AGGJ2019HUD::SetAmmo(FText&& AmmoText)
{
	if (mMainView)
	{
		mMainView->SetAmmoText(AmmoText);
	}
}
*/

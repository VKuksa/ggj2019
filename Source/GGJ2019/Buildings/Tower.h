// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

class UHealthComponent;

UCLASS()
class GGJ2019_API ATower : public AActor
{
	GENERATED_BODY()
	
    friend class AAIGod;
    
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    virtual void OnDeath(AActor*);
    
    virtual void OnDamaged(float, AActor*, AActor*);
    
    
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    
    virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
    
    UFUNCTION(BlueprintCallable) virtual void SetTarget (AActor * NewTarget) { Target = NewTarget; }
    
    UFUNCTION(BlueprintCallable) AActor * GetTarget () { return Target; }
    
    UFUNCTION(BlueprintImplementableEvent) void TargetChanged();
    
    UFUNCTION(BlueprintCallable) virtual bool IsAbleToAttack();
    
    UFUNCTION(BlueprintCallable) virtual void Attack();
    
    UFUNCTION(BlueprintImplementableEvent) void OnAttack();
    
protected:
    UPROPERTY(BlueprintReadWrite) AActor * Target;
    
    UPROPERTY(EditAnyWhere, BlueprintReadWrite) UHealthComponent * HealthComponent;
    
    UPROPERTY(EditAnyWhere) float DamageRadius = 100.f;
    
    UPROPERTY(EditAnyWhere) float DamageAmount = 10.f;
    
    UPROPERTY(EditAnyWhere) float MaxTime = 5.f;
    float CurTime = MaxTime;
    
};

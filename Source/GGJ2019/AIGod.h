// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AIGod.generated.h"

class ABaseEnemy;
class ATower;


UCLASS()
class GGJ2019_API AAIGod : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAIGod();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
    
    void RecalculateAllTargets();
    
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    
    void OnEnemiesDeath(AActor * DeadActor);
    void OnTargetDeath(AActor * DeadActor);
    
    void OnObjectSpawned(AActor * SpawnedObject);
    
    void FindTargetFor (ABaseEnemy * Object);
    
    void EndPlay(const EEndPlayReason::Type EndPlayReason);
   
    UPROPERTY(EditAnyWhere)
    TSubclassOf<ABaseEnemy> EnemyClass;
    
    
    UPROPERTY(EditAnyWhere)
    float DeathTime = 0.f;
    
    static TArray<ABaseEnemy*> Enemies;
    
    static TArray<ATower*> Targets;
    
    static const TArray<ATower*> & GetTowers() {return Targets;}
    static const TArray<ABaseEnemy*> & GetEnemies() {return Enemies;}
    
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseEnemy.generated.h"

class UHealthComponent;


UENUM(BlueprintType)
enum class ETargetType : uint8
{
    tPlayer,
    tBuildings
};


UCLASS()
class GGJ2019_API ABaseEnemy : public ACharacter
{
	GENERATED_BODY()

    friend class AAIGod;
public:
	// Sets default values for this character's properties
	ABaseEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
    
    UFUNCTION(BlueprintCallable) virtual void SetTarget (AActor * NewTarget) { Target = NewTarget; TargetChanged(); }

    UFUNCTION(BlueprintCallable) AActor * GetTarget () { return Target; }

    UFUNCTION(BlueprintImplementableEvent) void TargetChanged();

    UFUNCTION(BlueprintCallable) virtual bool IsAbleToAttack() { return true; }
    
    UFUNCTION(BlueprintCallable) virtual void Attack();
    
	UFUNCTION(BlueprintImplementableEvent) void OnDeath();

	UFUNCTION(BlueprintImplementableEvent) void OnAttack();
protected:
    virtual void OnDeath(AActor*);
    
    virtual void OnDamaged(float, AActor*, AActor*);
    
    
    UPROPERTY(EditAnyWhere) ETargetType Type;
    UPROPERTY(BlueprintReadWrite) AActor * Target;
    
    UPROPERTY(EditAnyWhere, BlueprintReadWrite) UHealthComponent * HealthComponent;
    
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "AIGod.h"

#include "BaseEnemy.h"

#include "Buildings/Tower.h"

#include "HealthComponent.h"

TArray<ABaseEnemy*> AAIGod::Enemies;

TArray<ATower*> AAIGod::Targets;

// Sets default values
AAIGod::AAIGod()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAIGod::BeginPlay()
{
	Super::BeginPlay();
	
    
    for (int i = 0; i < 10; ++i)
    {
        GetWorld()->SpawnActor<ABaseEnemy>(EnemyClass, FVector(FMath::RandRange(-1000, 1000), FMath::RandRange(-1000, 1000), FMath::RandRange(50, 200)), FRotator::ZeroRotator);
    }
    
}

// Called every frame
void AAIGod::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAIGod::RecalculateAllTargets()
{
    for (auto & Enemy : Enemies)
    {
        FindTargetFor(Enemy);
    }
}

void AAIGod::OnEnemiesDeath(AActor * DeadActor)
{
    Enemies.Remove(Cast<ABaseEnemy>(DeadActor));
    
    DeadActor->SetLifeSpan(DeathTime);
//    DeadActor->Destroy();
}

void AAIGod::OnTargetDeath(AActor * DeadActor)
{
    Targets.Remove(Cast<ATower>(DeadActor));
    DeadActor->SetLifeSpan(DeathTime);
    
    RecalculateAllTargets();
    //    DeadActor->Destroy();
}

void AAIGod::OnObjectSpawned(AActor * SpawnedObject)
{
    if (auto ObjectAsEnemy = Cast<ABaseEnemy>(SpawnedObject))
    {
        ObjectAsEnemy->HealthComponent->OnDeath.AddUObject(this, &AAIGod::OnEnemiesDeath);
        Enemies.Add(ObjectAsEnemy);
    }
    else
    {
        if (auto ObjectAsBuilding = Cast<ATower>(SpawnedObject))
        {
            ObjectAsBuilding->HealthComponent->OnDeath.AddUObject(this, &AAIGod::OnTargetDeath);
            // TODO: Possibly requires cast/ cast check
            Targets.Add(ObjectAsBuilding);
        }
    }
}

void AAIGod::FindTargetFor (ABaseEnemy * Object)
{
    AActor * BestTarget = nullptr;
    if (Targets.Num())
    {
        BestTarget = Targets[0];
        float Dist = FVector::Distance(BestTarget->GetActorLocation(), Object->GetActorLocation());
        for (auto & Target : Targets)
        {
            const float CurDist = FVector::Distance(Target->GetActorLocation(), Object->GetActorLocation());
            if (Dist > CurDist)
            {
                Dist = CurDist;
                BestTarget = Target;
            }
        }
        Object->SetTarget(BestTarget);
    }
}

void AAIGod::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
    Enemies.Empty();
    Targets.Empty();
}

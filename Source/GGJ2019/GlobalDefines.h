//
//  GlobalDefines.h
//  GGJ2019
//
//  Created by Dmytro Lavoryk on 1/26/19.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#ifndef GlobalDefines_h
#define GlobalDefines_h

#include "CoreMinimal.h"


namespace{
class SpawnManager
{
    friend class AIGod;
    
public:
    DECLARE_MULTICAST_DELEGATE_OneParam(FOnSpawned, AActor*);
private:
    
    static FOnSpawned OnSpawned;
    
public:
    
    
    static void ObjectSpawned (AActor * SpawnedObj)
    {
        OnSpawned.Broadcast(SpawnedObj);
    }
    
    
};

SpawnManager::FOnSpawned SpawnManager::OnSpawned;
}
#endif /* GlobalDefines_h */

// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthComponent.h"

namespace {
	static constexpr float MaxHP{ 100.f }, MinHP{ 0.f };
}

// Sets default values for this component's properties
UHealthComponent::UHealthComponent() : maxHealth(MaxHP), currentHealth(MaxHP)
{
	PrimaryComponentTick.bCanEverTick = true;
}

UHealthComponent::UHealthComponent(float maxHealth): maxHealth(maxHealth)
{
	PrimaryComponentTick.bCanEverTick = true;
}

float UHealthComponent::TakeDamage(float damage, AActor* damager)
{
	if (currentHealth > MinHP)
	{
		currentHealth -= damage;
		OnDamaged.Broadcast(damage, GetOwner(), damager);
		if (currentHealth <= MinHP)
		{
			OnDeath.Broadcast(GetOwner());
		}
		return damage;
	}
	return 0;
}

float UHealthComponent::GetMaxHealth() const
{
	return maxHealth;
}

float UHealthComponent::GetCurrentHealth() const
{
	return currentHealth;
}

void UHealthComponent::SetMaxHealth(float health)
{
	maxHealth = health;
}

void UHealthComponent::SetCurrentHealth(float health)
{
	currentHealth = health;
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
   // ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


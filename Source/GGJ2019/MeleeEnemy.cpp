// Fill out your copyright notice in the Description page of Project Settings.

#include "MeleeEnemy.h"

bool AMeleeEnemy::IsAbleToAttack()
{
    return Target
    && FVector::Distance(GetActorLocation(), Target->GetActorLocation()) <= Range;
}


void AMeleeEnemy::Attack()
{
    if (IsAbleToAttack())
    {
		Super::Attack();
        Target->TakeDamage(DamageAmount, FDamageEvent(), GetInstigatorController(), this); 
    }
}

void AMeleeEnemy::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Attack();
}

// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "GGJ2019GameMode.h"
#include "GGJ2019HUD.h"
#include "GGJ2019Character.h"
#include "UObject/ConstructorHelpers.h"

AGGJ2019GameMode::AGGJ2019GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGGJ2019HUD::StaticClass();
}

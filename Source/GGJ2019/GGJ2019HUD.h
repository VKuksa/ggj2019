// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GGJ2019HUD.generated.h"

class UMainView;

UCLASS()
class AGGJ2019HUD : public AHUD
{
	GENERATED_BODY()

public:
	void BeginPlay() override;
	AGGJ2019HUD();
	void SetHealth(FText && HealthText);
	void SetHealth(float HealthPercentage);
	//void SetAmmo(FText && AmmoText);
	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

protected:
	UPROPERTY(EditAnyWhere)
	TSubclassOf<UMainView> MainViewClass;


private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;
	UMainView * mMainView = nullptr;

};

